import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';
let products: Product[] = [
  { id: 1, name: 'Iphone', price: 10000 },
  { id: 2, name: 'Iphone', price: 10000 },
  { id: 3, name: 'Iphone', price: 10000 },
];
let lastProductId = 4;
@Injectable()
export class ProductService {
  create(CreateProductDto: CreateProductDto) {
    const newProduct: Product = {
      id: lastProductId++,
      ...CreateProductDto,
    };
    products.push(newProduct);
    return newProduct;
  }

  findAll() {
    return products;
    console.log('reset');
  }

  findOne(id: number) {
    const index = products.findIndex((Product) => {
      return Product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = products.findIndex((Product) => {
      return Product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const updateProduct: Product = {
      ...products[index],
      ...updateProductDto,
    };
    products[index] = updateProduct;
    return updateProduct;
  }

  remove(id: number) {
    const index = products.findIndex((Product) => {
      return Product.id === id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const deletedProducts = products[index];
    products.splice(index, 1);
    return deletedProducts;
  }
  reset() {
    products = [
      { id: 1, name: 'Iphone', price: 10000 },
      { id: 2, name: 'Iphone', price: 10000 },
      { id: 3, name: 'Iphone', price: 10000 },
    ];
    lastProductId = 4;
    console.log('reset');
  }
}
